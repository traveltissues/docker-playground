# -*- mode: ruby -*-
# vi: set ft=ruby :

CLIENT_SUBNET = "192.168.60."
NODE_SUBNET = "10.0.0."

CLIENT_MEM = 1024
NODE_MEM = 512

CLIENT_BOX = "bento/ubuntu-18.04"
NODE_BOX = "bento/ubuntu-18.04"

CLIENT_HOSTS = "client_hosts"
NODE_HOSTS = "node_hosts"

CLIENT_BOOK = "client.yml"
SWM_BOOK = "swarm.yml"

CLIENTS = [
  {
    :name => "docker20.dev",
    :box => CLIENT_BOX,
    :memory => CLIENT_MEM,
    :ip => CLIENT_SUBNET + "20",
    :playbook => CLIENT_BOOK,
    :inventory => CLIENT_HOSTS
  }
]

NODES = [
  {
    :name => "mgr1",
    :box => NODE_BOX,
    :memory => NODE_MEM,
    :ip => NODE_SUBNET + "2",
    :playbook => SWM_BOOK,
    :inventory => NODE_HOSTS
  },
  {
    :name => "mgr2",
    :box => NODE_BOX,
    :memory => NODE_MEM,
    :ip => NODE_SUBNET + "3",
    :playbook => SWM_BOOK,
    :inventory => NODE_HOSTS
  },
  {
    :name => "mgr3",
    :box => NODE_BOX,
    :memory => NODE_MEM,
    :ip => NODE_SUBNET + "4",
    :playbook => SWM_BOOK,
    :inventory => NODE_HOSTS
  },
  {
    :name => "wrk1",
    :box => NODE_BOX,
    :memory => NODE_MEM,
    :ip => NODE_SUBNET + "5",
    :playbook => SWM_BOOK,
    :inventory => NODE_HOSTS
  },
  {
    :name => "wrk2",
    :box => NODE_BOX,
    :memory => NODE_MEM,
    :ip => NODE_SUBNET + "6",
    :playbook => SWM_BOOK,
    :inventory => NODE_HOSTS
  },
  {
    :name => "wrk3",
    :box => NODE_BOX,
    :memory => NODE_MEM,
    :ip => NODE_SUBNET + "7",
    :playbook => SWM_BOOK,
    :inventory => NODE_HOSTS
  }
]

Vagrant.configure("2") do |config|

  if ENV["SWARM"] == "1" then
    machines = NODES
  else
    machines = CLIENTS
  end

  machines.each do |opts|
    config.vm.define opts[:name] do |node|
      node.vm.box = opts[:box]
      node.ssh.insert_key = false
      node.vm.provider :virtualbox do |v|
        v.linked_clone = true
        v.memory = opts[:memory]
      end
      node.vm.hostname = opts[:name]
      node.vm.network :private_network, ip: opts[:ip]
      if (opts.has_key?(:host_port)) and (opts.has_key?(:guest_port)) then
        node.vm.network :forwarded_port, guest: opts[:guest_port], host: opts[:host_port], host_ip: "127.0.0.1"
      end
      node.vm.provision :ansible do |a|
        a.playbook = opts[:playbook]
        a.inventory_path = opts[:inventory]
        a.galaxy_role_file = "roles.yml"
        a.extra_vars = {
          ansible_ssh_user: "vagrant",
          ansible_ssh_private_key_file: "~/.vagrant.d/insecure_private_key",
          ansible_host: opts[:ip]
        }
      end
    end
  end
end
